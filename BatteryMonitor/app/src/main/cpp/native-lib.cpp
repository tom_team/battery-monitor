#include <jni.h>
#include <string>
#include <android/log.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>


extern "C"
JNIEXPORT jstring JNICALL
Java_com_battery_gathin_batterymonitor_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT void JNICALL
Java_com_battery_gathin_batterymonitor_MainActivity_ScreenShotFromJNI(
        JNIEnv* env,
        jobject /* this */) {

    std::string hello = "Hello from C++";
    __android_log_print(ANDROID_LOG_INFO, "JNIMsg", "TOMTOM ScreenShotFromJNI");
    return;
}
