package com.battery.gathin.batterymonitor;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.battery.gathin.batterymonitor.util.batteryUtil;
import com.battery.gathin.batterymonitor.util.sendEmailUtil;
import com.battery.gathin.batterymonitor.wifi.WiFiInfo;

import java.io.IOException;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";
    private boolean mIsResume = false;
    private boolean mIsshowWiFiRssi= false;
    private batteryUtil mBatteryUtil;
    private sendEmailUtil mEmail;
    private Context mContext;

    private void init() {
        mContext = getApplicationContext();
        mBatteryUtil = new batteryUtil(mContext);
        mEmail = new sendEmailUtil(mContext);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
        setContentView(R.layout.activity_main);
        setToolBar();
        setFloatingActionButton();
        setBatteryColorImage();
    }

    private static int mIndexColor = 0;
    private static int mColor = Color.BLACK;
    private void setBatteryColorImage() {
        ImageView iv = (ImageView) findViewById(R.id.battery_color);
        iv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                switch (mIndexColor % 11) {
                    case 0:
                        mColor = Color.BLACK;
                        break;
                    case 1:
                        mColor = Color.DKGRAY;
                        break;
                    case 2:
                        mColor = Color.GRAY;
                        break;
                    case 3:
                        mColor = Color.LTGRAY;
                        break;
                    case 4:
                        mColor = Color.WHITE;
                        break;
                    case 5:
                        mColor = Color.RED;
                        break;
                    case 6:
                        mColor = Color.GREEN;
                        break;
                    case 7:
                        mColor = Color.BLUE;
                        break;
                    case 8:
                        mColor = Color.YELLOW;
                        break;
                    case 9:
                        mColor = Color.CYAN;
                        break;
                    case 10:
                        mColor = Color.MAGENTA;
                        break;
                }


                setBatteryInfoText(mColor, true);
                mIndexColor++;
            }
        });
    }

    private void setFloatingActionButton() {
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Send Email to Developer", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                mEmail.sendEmail();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_screenshot) {
            try {
                Log.i(TAG,"============= action_settings =======================");
                checkPermission();
                playsound();
                Process process = Runtime.getRuntime().exec(
                        "/system/bin/screencap -p /sdcard/Pictures/Screenshots/battery.png");
                Toast.makeText(mContext, "Screenshot into /sdcard/Pictures/Screenshots/battery.png", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        } else if (id == R.id.action_wifiRssi) {
            mIsshowWiFiRssi = true;
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
            } else {
                //do something
            }
        } else {
            //do something
        }
    }

    private void playsound() {
        final MediaPlayer[] _shootMP = {null};
        AudioManager manager = (AudioManager)
                getSystemService(Context.AUDIO_SERVICE);
        manager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,
                manager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        if (_shootMP[0] == null)
            _shootMP[0] = MediaPlayer
                    .create(mContext,
                            Uri.parse("file:///system/media/audio/ui/camera_click.ogg"));
        if (_shootMP[0] != null) {
            try {

                _shootMP[0].start();
                _shootMP[0].setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer arg0) {
                        // release the media
                        _shootMP[0].stop();
                        _shootMP[0].reset();
                        _shootMP[0].release();
                        _shootMP[0] = null;

                    }
                });
            } catch (IllegalStateException e) {
                Log.w(TAG, "Exception takeScreenShot" + e.getMessage());

            }
        }
    }

    @Override
    protected void onResume() {
        Log.i(TAG,"============= BatteryInfo  onResume =======================");
        super.onResume();
        mIsResume = true;
        printBatteryInfomation();
        ScreenShotFromJNI();
    }

    @Override
    protected void onPause() {
        Log.i(TAG,"============= BatteryInfo  onPause =======================");
        super.onPause();
        mIsResume = false;
    }

    private void printBatteryInfomation() {
        new Thread(new Runnable() {
            public void run() {
                while (mIsResume) {
                    Log.i(TAG,"=============  Set BatteryInfo =======================");
                    mHandler.sendEmptyMessage(0);
                    sleepMinSecond(2000);
                }
            }

            private void sleepMinSecond(int i) {
                try {
                    Thread.sleep(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            setBatteryInfoText(mColor, true);
            showWiFiRssi();
        }
    };

    private void showWiFiRssi() {
        if (mIsshowWiFiRssi) {
            int wifi[] = new WiFiInfo(mContext).getWiFiRssi();
            Toast.makeText(mContext, "WiFi Level: " + wifi[0] + " ,RSSI:" + wifi[1] +" dbm" , Toast.LENGTH_SHORT).show();
        }
    }

    private void setBatteryInfoText(int color, boolean update) {
        String bi[] = {};
        if (update)
            bi = batteryUtil.getBatteryInfo();

        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.full_capacity);
        tv.setTextColor(color);
        if (update)
            tv.setText(bi[0]);

        tv = (TextView) findViewById(R.id.remaining_capacity);
        tv.setTextColor(color);
        if (update)
            tv.setText(bi[1]);

        tv = (TextView) findViewById(R.id.battery_level);
        tv.setTextColor(color);
        if (update)
            tv.setText(bi[2]);

        tv = (TextView) findViewById(R.id.battery_voltage);
        tv.setTextColor(color);
        if (update)
            tv.setText(bi[3]);

        tv = (TextView) findViewById(R.id.battery_temp);
        tv.setTextColor(color);
        if (update)
            tv.setText(bi[4]);

        tv = (TextView) findViewById(R.id.battery_curr);
        tv.setTextColor(color);
        if (update)
            tv.setText(bi[5]);

        tv = (TextView) findViewById(R.id.battery_status);
        tv.setTextColor(color);
        if (update)
            tv.setText(bi[6]);

        tv = (TextView) findViewById(R.id.battery_charger_type);
        tv.setTextColor(color);
        if (update)
            tv.setText(bi[7]);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
    public native void ScreenShotFromJNI();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
}
