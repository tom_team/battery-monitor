package com.battery.gathin.batterymonitor.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


/**
 * Created by gathin on 2017/10/21.
 */

public class sendEmailUtil {
    private final static String TAG = "sendEmailUtil";
    private final static String DEVELOPER_EMAIL = "gathin12345@gmail.com";
    private final static String SUBJECT_EMAIL   = "Subject: leave your segection to Developer";
    private final static String TEXT_EMAIL      = "Leave your comment";
    private final static String CREATE_CHOOSER  = "Choose Email type";
    private final static String ERROR_EAMIL     = "There are no email clients installed.";

    private static Context mContext;

    public sendEmailUtil(Context context) {
        mContext = context;
    }

    public void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{DEVELOPER_EMAIL} );
        intent.putExtra(Intent.EXTRA_SUBJECT, SUBJECT_EMAIL);
        intent.putExtra(Intent.EXTRA_TEXT, TEXT_EMAIL);

        Intent shareIntent = Intent.createChooser(intent, CREATE_CHOOSER);
        shareIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);

        try {
            mContext.startActivity(shareIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(mContext, ERROR_EAMIL, Toast.LENGTH_SHORT).show();
        }
    }
}
