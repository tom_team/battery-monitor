package com.battery.gathin.batterymonitor.util;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by gathin on 2017/10/21.
 */

public class batteryUtil {
    private final static String TAG = "batteryUtil";
    private static Context mContext;
    private final static String CURRENT_NOW_PATH = "/sys/class/power_supply/battery/current_now";
    private final static String CURRENT_NOW_LTI20_PATH = "/sys/class/power_supply/bq27520/current_now";

    public batteryUtil(Context context) {
        mContext = context;
    }

    public synchronized static String[] getBatteryInfo() {
        BatteryManager BM = (BatteryManager) mContext.getSystemService(Context.BATTERY_SERVICE);
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = mContext.registerReceiver(null, ifilter);

        int batteryVoltage = batteryStatus.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
        int batteryTemp    = batteryStatus.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
        int batterylevel   = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int status         = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        int plugtype       = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

        // get battery full capacity
        int fullcapacity = (int) getFullBatteryCapacity();

        // get battery remaining capacity
        int remaingcapacity = fullcapacity * batterylevel/100;


        // get battery current
        int current_now = -1;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            current_now = BM.getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);
        } else {
            try {
                current_now = Integer.valueOf(readFromFile(CURRENT_NOW_PATH));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                current_now = getSpecificPathCuurent();
            }
        }

        // get battery status
        String Batterystatus;
        switch (status) {
            case BatteryManager.BATTERY_STATUS_CHARGING:
                Batterystatus = "充電中";
                break;
            case BatteryManager.BATTERY_STATUS_DISCHARGING:
                Batterystatus = "放電中";
                break;
            case BatteryManager.BATTERY_STATUS_FULL:
                Batterystatus = "電池全滿";
                break;
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                Batterystatus = "電池不再充電中";
                break;
            case BatteryManager.BATTERY_HEALTH_UNKNOWN:
            default:
                Batterystatus = "電池未知狀態";
                break;
        }

        // get battery charger type
        String BatteryChargerType;
        switch (plugtype) {
            case BatteryManager.BATTERY_PLUGGED_AC:
                BatteryChargerType = "AC 充電";
                break;
            case BatteryManager.BATTERY_PLUGGED_USB:
                BatteryChargerType = "USB 充電";
                break;
            case BatteryManager.BATTERY_PLUGGED_WIRELESS:
                BatteryChargerType = "Wireless 充電";
                break;
            default:
                BatteryChargerType = "沒充電";
                break;
        }

        Log.d(TAG, "TOMTOM battery info" +
                "status: " + Batterystatus + " ," +
                "plugtype: " + BatteryChargerType + " ," +
                "remaingcapacity: " + remaingcapacity + " ," +
                "batterylevel: " + batterylevel + " ," +
                "batteryVoltage: " + batteryVoltage + " ," +
                "batteryTemp: " + batteryTemp + " ," +
                "current_now: " + current_now);

        return new String[]{
                String.valueOf(fullcapacity) + " mAh",
                String.valueOf(remaingcapacity) + " mAh",
                String.valueOf(batterylevel) + " %",
                String.valueOf(batteryVoltage) + " mV",
                String.valueOf(batteryTemp/10) + "."+ String.valueOf(batteryTemp%10) + " C",
                String.valueOf(current_now > 1000 ?
                        current_now/1000 : current_now < -1000 ? current_now/1000 : current_now) + " mA",
                Batterystatus,
                BatteryChargerType
        };
    }

    private static int getSpecificPathCuurent() {
        String product = Build.MODEL;
        int current_now = 0;

        if ("LT26i".equals(product)) {
            current_now = Integer.valueOf(readFromFile(CURRENT_NOW_LTI20_PATH));
        }

        return current_now;
    }

    private static double getFullBatteryCapacity() {
        Object mPowerProfile;
        double batteryCapacity = 0;
        final String POWER_PROFILE_CLASS = "com.android.internal.os.PowerProfile";

        try {
            mPowerProfile = Class.forName(POWER_PROFILE_CLASS)
                    .getConstructor(Context.class)
                    .newInstance(mContext);

            batteryCapacity = (double) Class
                    .forName(POWER_PROFILE_CLASS)
                    .getMethod("getBatteryCapacity")
                    .invoke(mPowerProfile);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return batteryCapacity;
    }


    public static String readFromFile(String nameFile) {
        String aBuffer = "";
        try {
            File myFile = new File(nameFile);
            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(new InputStreamReader(fIn));
            String aDataRow = "";
            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow;
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return aBuffer;
    }

}
