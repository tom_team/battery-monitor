package com.battery.gathin.batterymonitor.wifi;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

/**
 * Created by gathin on 2017/11/3.
 */

public class WiFiInfo {
    private final static String TAG = "WiFiInfo";
    private static Context mContext;

    public WiFiInfo(Context context) {
        mContext = context;
    }

    public int[] getWiFiRssi() {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int numberOfLevels = 5;
        int rssi = wifiInfo.getRssi();
        int level = WifiManager.calculateSignalLevel(rssi, numberOfLevels);

        return new int[] {level , rssi};
    }
}
